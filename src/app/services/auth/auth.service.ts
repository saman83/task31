import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private user: any = null;

  constructor() { }

  public login(user: any): boolean  {
    if (user.username == 'saman' && user.password == 'sandy') {
      this.user = {...user};
    } else {
      this.user = null;
    }

    return this.isLoggedIn();
  } 

  public isLoggedIn(): boolean {
    return this.user !== null;
    
  }

  public register(user:any): boolean {
    if (user.username && user.password) {
      this.user = {...user};
    } else {
      this.user = null;
    }

    return this.isLoggedIn();
  }
}
