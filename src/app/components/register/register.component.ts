import { AuthService } from './../../services/auth/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user = {
    username: '',
    password: ''
  };
  
  constructor(private registerService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  onRegisterClicked() {

    const registerDone = this.registerService.register(this.user);

    console.log(registerDone);

    if (registerDone) {
      this.router.navigateByUrl('/dashboard');
    } else {
      alert('Eyy, you need to register first..')
    }
    
  }
}
