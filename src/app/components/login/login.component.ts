import { AuthService } from './../../services/auth/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user = {
    username: '',
    password: ''
  };

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  onLoginClicked() {
    
    const succes = this.authService.login(this.user);

    console.log(succes);
    

    if (succes) {
      this.router.navigateByUrl('/dashboard');
    } else {
      alert('Eyy, I dont know you')
    }
    
  }

}
